import { store } from '../App';
import { updateclientStatus } from '../actions/Index';

const serverURI = 'https://api.sveet.io/';

export const sendRequest = (endpoint, requestData, token) => fetch(serverURI + endpoint, {
  method: 'POST',
  body: JSON.stringify(requestData),
  headers: {
    Authorization: 'Bearer ' + token,
    'Content-type': 'application/json',
  },
  credentials: 'same-origin',
})
  .then(response => response.json())
  .then((data) => {
    store.dispatch(updateclientStatus(true));
    return data;
  })
  .catch((error) => {
    store.dispatch(updateclientStatus(false));
    console.error(error);
  });


export default sendRequest;
