import { sendRequest } from './APICall.js';

export const cartValue = (cart, order) => {
  let amount = 0;
  order.forEach((item) => {
    amount += item.item.item_price * item.item_count;
  });
  cart.forEach((item) => {
    amount += item.item.item_price * item.item_count;
  });
  return amount;
};

export const findItemByID = (menu, itemId) => {
  let itemInfo = '';
  // Check in featured categories
  menu.featured.forEach((item) => {
    if (item.item_id === itemId) {
      itemInfo = item;
    }
  });
  if (!itemInfo) {
    // Check in all sections
    Object.keys(menu.categories).map((key) => {
      const subCategories = menu.categories[key].sub_categories;

      Object.keys(subCategories).map((subCategory) => {
        const itemsData = subCategories[subCategory].items;

        // Loop in section data
        itemsData.forEach((item) => {
          if (item.item_id === itemId) itemInfo = item;
        });
      });

      return 1;
    });
  }
  return itemInfo;
};

// Return item by item_id
export const findItemIDBYCount = (cart, itemId) => {
  // Check in featured categories
  let itemCount = 0;
  let itemIndex = -1;
  let matchIndex = -1;
  cart.forEach((item) => {
    itemIndex++;
    if (item.item.item_id === itemId) {
      itemCount++;
      matchIndex = itemIndex;
    }
  });

  return matchIndex;
};

// Return item by item_id
export const differentItem = (cart, itemId) => {
  // Check in featured categories
  let itemCount = 0;
  cart.forEach((item) => {
    if (item.item.item_id === itemId) {
      itemCount++;
    }
  });

  return itemCount;
};

export const getTotalNoItem = (cart, order) => {
  // Check in featured categories
  let itemCount = 0;
  cart.forEach((item) => {
    itemCount += item.item_count;
  });
  order.forEach((item) => {
    itemCount += item.item_count;
  });

  return itemCount;
};

export const itemCount = (cart, itemID, customizations) => {
  let itemCount = 0;
  if (customizations) {
    cart.forEach((item) => {
      if (item.item.item_id === itemID
        && JSON.stringify(item.customizations) === JSON.stringify(customizations)) {
        itemCount += item.item_count;
      }
    });
  }
  else {
    for (const item in cart) {
      if (cart[item].item.item_id === itemID) {
        itemCount += cart[item].item_count;
      }
    }
  }
  return itemCount;
};

export const checkConnection = () => {
  const requestJSON = {
    table_code: 'ABCD',
  };
  return sendRequest('getCafeData', requestJSON);
};
