/**
 * Action to manage cart and its items.
 * @version 1.0.0
 * Date Created - 22/08/2018
 */

import { sendRequest } from '../services/APICall';


export const CREATE_SESSION = 'CREATE_SESSION';
export const CREATE_MENU = 'CREATE_MENU';
export const CHANGE_VEG_FILTER = 'CHANGE_VEG_FILTER';
export const CHANGE_CATEGORY_FILTER = 'CHANGE_CATEGORY_FILTER';
export const CREATE_ORDER = 'CREATE_ORDER';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const UPDATE_CLIENT_STATUS = 'UPDATE_CLIENT_STATUS';
export const ADD_USER_DATA = 'LOGIN_USER';
export const UPDATE_USER_LOGIN = 'UPDATE_USER_LOGIN';
export const ADD_CUSTOMER = 'ADD_CUSTOMER';
export const PLACE_ORDER = 'PLACE_ORDER';
export const UPDATE_CART = 'UPDATE_CART';
export const EMPTY_CART = 'EMPTY_CART';
export const EMPTY_ORDER = 'EMPTY_ORDER';
export const UPDATE_ORDER_ID = 'UPDATE_ORDER_ID';
export const ADD_ITEM_TO_ORDER = 'ADD_ITEM_TO_ORDER';
export const UPDATE_ORDER_STATUS = 'UPDATE_ORDER_STATUS';
export const CHECK_REWARDS = 'CHECK_REWARDS';
export const APPLY_REWARDS = 'APPLY_REWARDS';
export const ADD_FEEDBACK = 'ADD_FEEDBACK';
/*
 * other constants
 */

export const VegFilters = {
  VEG: 'VEG',
  NON_VEG: 'NON_VEG',
};

/*
 * action creators to dispatch state change in reducers
 */

// Translation method to convert to backend compaitable JSON.

function convertJSON(oldFormat) {
  var newFormat = JSON.parse(JSON.stringify(oldFormat));
  newFormat.items.forEach((item, itemIndex) => {
    newFormat.items[itemIndex].customizations.selectedCustomization = [];
    console.log('asd')
  })
  oldFormat.items.forEach((item, itemIndex) => {
    for (var customizationName in item.customizations.selectedCustomization) {
      let customizationData = item.customizations.selectedCustomization[customizationName];
      let tempCustomization = {
        name: customizationName,
        customization: []
      }
      if (customizationData.length === undefined) {
        tempCustomization.customization.push({
          name: customizationData.value,
          amount: customizationData.amount
        })
      } else {
        customizationData.forEach((customizationOption) => {
          tempCustomization.customization.push({
            name: customizationOption.value,
            amount: customizationOption.amount
          });
        });
      }
      newFormat.items[itemIndex].customizations.selectedCustomization.push(tempCustomization);
    }
  });
  return newFormat;
}

/*
 * Fetch store data
 */

function fetchStoreData(tableCode) {
  const requestJSON = {
    table_code: tableCode,
  };
  return sendRequest('getCafeData', requestJSON);
}


function transactionCheck(transactionID, type, orderID, token) {
  if(type === 'cash')
  {
    const requestJSON = {
      transaction_id: transactionID,
      order_id: Number(orderID),
      type: 'cash'
    };
    return sendRequest('requestCash', requestJSON, token);
  }
  else{
    const requestJSON = {
      transaction_id: transactionID,
      order_id: Number(orderID),
    };
    return sendRequest('transactionSuccessCheck', requestJSON, token);
  }

}
/*
 * Fetch store data
 */

function pushOrder(data, customer, cafeID, tableCode) {
  const requestJSON = {
    table_code: tableCode,
    cafe_id: cafeID,
    user_id: customer.user_id,
    items: data,
  };
  
  return sendRequest('placeOrder', convertJSON(requestJSON), customer.token);
}

/*
 * Fetch store data
 */

function pushItemsToOrder(data, customer, cafeID, tableCode, orderID) {
  data.forEach((itemData) => {
    if (itemData.customizations) {
      itemData.customizations = [itemData.customizations];
    }
  });
  const requestJSON = {
    id: orderID,
    table_code: tableCode,
    cafe_id: cafeID,
    user_id: customer.user_id,
    items: data,
  };
  return sendRequest('updateOrder', convertJSON(requestJSON), customer.token);
}

export function completeOrder(transactionID, type, orderID,token) {
  if(type=="cash") {
    transactionCheck(transactionID,"cash", '', token);
    return destorySession();
  }
  else{
    transactionCheck(transactionID,"type", orderID, token);
    return destorySession();
  }
}

// Fetch User Session and update the state
export function createSession(tableCode, userLoggedIn) {
  if (!userLoggedIn) {
    userLoggedIn = 'no';
  }
  return (dispatch) => {
    const requestData = fetchStoreData(tableCode);
    return requestData.then((data) => {
      if (data) {
        data.menu.is_veg_only = false;
        dispatch({ type: CREATE_MENU, data: data.menu });
        // Process data
        return dispatch({
          type: CREATE_SESSION,
          table_code: tableCode,
          data: data.cafe_info,
          user_logged_in: userLoggedIn,
        });
      }
      return 1;
    });
  };
}

// Fetch User Session and update the state
export function updateVegFilter(isVeg) {
  return dispatch => dispatch({ type: CHANGE_VEG_FILTER, data: isVeg });
}

// Change veg/non-veg filter. Default is veg
export function changeVegFilter() {
  return { type: CHANGE_VEG_FILTER };
}

// Sets the category filter
export function setCategoryFilter(categoryName) {
  return { type: CHANGE_CATEGORY_FILTER, categoryName };
}

// Add item to cart
export function addToCart(item, customizations) {
  return { type: ADD_TO_CART, item, customizations };
}

// Add item to cart
export function updateCart(cart) {
  return { type: UPDATE_CART, cart };
}

// Add user details to session
export function loginUser(phone, userID, token) {
  return (dispatch) => {
    dispatch({ type: UPDATE_USER_LOGIN, user_logged_in: 'yes' });
    return dispatch({
      type: ADD_USER_DATA, phone, user_id: userID, token,
    });
  };
}

// Add user details to session
export function destorySession() {
  return (dispatch) => {
    dispatch({ type: UPDATE_USER_LOGIN, user_logged_in: 'no' });
    dispatch({ type: UPDATE_ORDER_ID, order_id: '' });
    dispatch({ type: EMPTY_CART });
    dispatch({ type: EMPTY_ORDER });
  };
}

// Add item to cart
export function createOrder(data, customer, cafeID, tableCode) {
  return (dispatch) => {
    pushOrder(data, customer, cafeID, tableCode).then((responseData) => {
      if (responseData) {
        const orderID = responseData.id;
        dispatch({ type: UPDATE_ORDER_ID, order_id: orderID });
        dispatch({ type: EMPTY_CART });
        dispatch({ type: CREATE_ORDER, data });
      }
    });
  };
}

// Add Item to order
export function updateclientStatus(type) {
  return (dispatch) => {
    dispatch({ type: UPDATE_CLIENT_STATUS, data: type });
  };
}

// Add Item to order
export function addItemToOrder(data, customer, cafeID, tableCode, orderID) {
  return (dispatch) => {
    pushItemsToOrder(data, customer, cafeID, tableCode, orderID).then((responseData) => {
      if (responseData) {
        Object.keys(data).forEach((item) => {
          const itemData = data[item];
          dispatch({ type: ADD_ITEM_TO_ORDER, item_data: itemData });
        });
        dispatch({ type: EMPTY_CART });
      }
    });
  };
}


// Removes item from cart
export function removeFromCart(index) {
  return { type: REMOVE_FROM_CART, index };
}

// Place an order
export function placeOrder() {
  return { type: PLACE_ORDER };
}

// Place an order
export function addCustomer() {
  return { type: ADD_CUSTOMER };
}


// Updates the order status. Use while payment
export function updateOrderStatus(status) {
  return { type: UPDATE_ORDER_STATUS, status };
}

// Check the existing rewards if exists
export function checkRewards() {
  return { type: CHECK_REWARDS };
}

// Apply rewards to the status
export function applyRewards() {
  return { type: APPLY_REWARDS };
}

// Add the feedback in the app
export function addFeedback(rating) {
  return { type: ADD_FEEDBACK, rating };
}
