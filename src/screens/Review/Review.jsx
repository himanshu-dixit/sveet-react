import React, { Component } from 'react';
import './Review.css';
import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { sendRequest } from '../../services/APICall';

import OTPBox from '../../components/standard/OTPBox/OTPBox';

import BackButton from '../../components/standard/BackButton/BackButton';
import Button from '../../components/standard/Button';

import {
  createSession,
  updateCart,
  loginUser,
  addToCart,
  createOrder,
  addItemToOrder,
  removeFromCart,
} from '../../actions/Index';

import SimpleMenuItem from '../../components/standard/SimpleMenuItem/SimpleMenuItem';
import { itemCount, findItemByID, cartValue } from '../../services/Menu';

window.$ = window.jQuery = require('jquery');

class Review extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    window.jQuery([document.documentElement, document.body]).animate({
      scrollTop: 0,
    }, 10);
  }

  render() {
    // Variable to store data
    const storeInfo = this.props.store.store || '';

    const that = this;

    // Variable to store data
    const menu = this.props.menu || '';
    const cart = this.props.cart || '';
    const order = this.props.order || '';
    let itemIndex = -1;


    if (this.state.redirect) {
      return <Redirect push to="/order/" />;
    }

    if (this.state.menu_redirect) {
      return <Redirect push to={'/menu/' + this.props.store.table_code} />;
    }
    // Create list of categories for menu
    const categoriesList = ['Featured'];

    for (const k in this.props.menu.categories) categoriesList.push(k);


    // can be used to load loading screen
    if (!(this.props.store.menu === '')) {
      const that = this;

      // Set open state for customization box
      function openOTPBox() {
        if (!that.state.OTPBoxVisible) that.setState({ OTPBoxVisible: 1 });
      }

      function discard() {
        that.setState({ OTPBoxVisible: 0 });
      }

      // Used to add item to cart state by dispatching function
      function removeItemfromCart(index) {
        that.props.removeFromCart(index);
      }

      // Place order
      function placeOrder() {
        // Check if user is logged in
        if (that.props.store.user_logged_in === 'yes') {
          // Add user details to the order
          checkout();
        }
        else {
          // show customization box
          openOTPBox();
        }
      }


      // Used to add item to cart state by dispatching function
      function addItemToCart(itemId, customization) {
        // Deep copy item
        const item = JSON.parse(JSON.stringify(findItemByID(menu, itemId)));
        // If no customization or customization has been done.
        if (customization) item.item_price += customization.total;
        that.props.addToCart(item, customization || '');
        // Ensure customization box is closed
        that.setState({ customizationBoxVisible: 0, item_active: '' });
      }

      function floorFigure(figure, decimals) {
        if (!decimals) decimals = 2;
        const d = Math.pow(10, decimals);
        return (parseInt(figure * d) / d).toFixed(decimals);
      }

      function checkout() {
        // Save to order from cart && redirect
        console.log(that.props.store);
        if (that.props.order.length === 0) {
          that.props.createOrder(
            that.props.cart,
            that.props.customer,
            that.props.store.store.cafe_id,
            that.props.store.table_code,
          );
        }
        else if (cart.length !== 0) {
          that.props.addItemToOrder(
            cart,
            that.props.customer,
            that.props.store.store.cafe_id,
            that.props.store.table_code,
            that.props.store.order_id,
          );
        }
        // Send Cafe_id, phone_no and order
        // order
        // this.props.customer.user_info
        // storeInfo.cafe_id
        // Redirect to order page

        that.setState({ redirect: true });
      }

      function startUserSession(phone_no, user_id, token) {
        that.setState({ OTPBoxVisible: 0 });
        that.props.loginUser(phone_no, user_id, token);
        checkout();
      }

      function checkConnection() {
        const requestJSON = {
          table_code: 'ABCD',
        };
        return sendRequest('getCafeData', requestJSON);
      }

      function doNothing() {
        return 1;
      }

      // If user is not connected to internet
      if (this.props.store.clientstatus === false) {
        return (
          <div className="Error" onClick={() => { checkConnection(); }}>
            <div className="Image-top">
              <img
                src={require('../../assets/images/icons/no-internet.png')}
                width="100%"
                alt="Error"
                className="No-internet-icon"
              />
              <div className="No-internet">
                No Internet Connection
              </div>
            </div>
            <Button
              align="center"
              color="Red"
              text="Try Again"
              type="simple"
              onClickEvent={() => { checkConnection(); }}
              border
            />
          </div>
        );
      }

      return (
        <div className="Review">
          <div className="Top-bar">
            <div className="Background">

            </div>

            <div className="Topbar-content">
              <div className="Cafe-logo">
                <BackButton screen={'/menu/' + this.props.store.table_code} />
                {storeInfo.cafe_name}
              </div>
            </div>
          </div>


          {(cart.length || order.length) ? (
            <div className="Bottom-float-button">
              <Button
                align="center"
                color="Red"
                text="Place Order"
                type="simple"
                onClickEvent={placeOrder}
                border
              />
            </div>
          ) : (
            <div className="Bottom-float-button">
              <Button
                align="center"
                color="Red"
                text="Add more items"
                type="simple"
                onClickEvent={() => { that.setState({ menu_redirect: true }); }}
                border
              />
            </div>
          )}

          <div className="Custimzation-box">
            {this.state.OTPBoxVisible === 1 ? <OTPBox startUserSession={startUserSession} OnDiscard={discard.bind(this)} /> : ''}

          </div>

          <div className="Container">

            {cart.length || order.length ? (
              <div className="Inner-Container">

                <div className="Item-heading">Current Order</div>

                {
                cart.map(function (Item_info) {
                  itemIndex++;
                  const item = Item_info.item;
                  const customizations = Item_info.customizations;
                  return (
                    <SimpleMenuItem
                      store_id={storeInfo.cafe_id}
                      key={item.item_id + Math.random()}
                      item_id={item.item_id}
                      name={item.item_name}
                      type="review"
                      customization={customizations}
                      item_count={itemCount(cart, item.item_id, customizations)}
                      OnDelete={removeItemfromCart.bind(this, itemIndex)}
                      OnClick={addItemToCart.bind(this)}
                      isVeg={item.is_veg}
                      price={item.item_price}
                      description={item.item_desc}
                      image_url={item.item_pic}
                    />
                  );
                })
              }
                {order.length
                  ? <div className="Item-heading">Previous Order</div>
                  : ''}

                {
                order.map((Item_info) => {
                  const item = Item_info.item;
                  const customizations = Item_info.customizations;
                  return (
                    <SimpleMenuItem
                      store_id={storeInfo.cafe_id}
                      key={item.item_id + Math.random()}
                      item_id={item.item_id}
                      name={item.item_name}
                      show_button="no"
                      OnClick={addItemToCart.bind(this)}
                      OnDelete={doNothing.bind(this, itemIndex)}
                      customization={customizations}
                      item_count={itemCount(order, item.item_id, customizations)}
                      type="review"
                      isVeg={item.is_veg}
                      price={item.item_price}
                      image_url={item.item_pic}
                    />
                  );
                })
              }
                <div className="Item-total-bar">

                  <div className="Total">
                  Item Total
                  </div>

                  <span className="Total-price">
                  ₹
                    {' '}
                    {cartValue(cart, order)}
                  </span>
                </div>
                <div className="Tax-total-bar">

                  <div className="Total">
                  CGST - 2.5%
                  </div>

                  <span className="Total-price">
                   ₹
                    {' '}
                    {floorFigure((cartValue(cart, order) * 0.025))}
                  </span>
                </div>
                <div className="Tax-total-bar">

                  <div className="Total">
                  SGST - 2.5%
                  </div>

                  <span className="Total-price">
                    ₹
                    {' '}
                    {floorFigure(cartValue(cart, order) * 0.025)}
                  </span>
                </div>
                <div className="Total-bar">

                  <div className="Total">
                  Total
                  </div>

                  <span className="Total-price">
                  ₹
                    {' '}
                    {floorFigure(cartValue(cart, order) * 1.05)}
                  </span>
                </div>
                {/* Iterate over sections */}

              </div>
            ) : (
              <div className="Empty-cart-box">
                <img
                  src={require('../../assets/images/icons/no-item.png')}
                  className="Empty-cart"
                  alt="Empty cart"
                />
              </div>
            )}

          </div>


        </div>
      );
    }

    return (
      <div>
        Loading
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    store: state.user_session,
    menu: state.menu,
    cart: state.cart,
    order: state.order,
    customer: state.customer,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createSession, updateCart, addToCart, loginUser, createOrder, addItemToOrder, removeFromCart,
  }, dispatch); // createSession()(dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Review);
