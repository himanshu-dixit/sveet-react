import React, { Component } from 'react';

import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import QrReader from 'react-qr-reader'

import Button from '../../components/standard/Button';
import InputField from '../../components/standard/InputField';
import Error from '../../components/standard/Error/Error';

import './TableCode.css';

class TableCode extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      value: '',
      delay: 500,
      result: 'No result'
    };
    
    this.handleQRScan = this.handleQRScan.bind(this)
    this.openImageDialog = this.openImageDialog.bind(this)
  
    // Create ref for QR code component
    this.qrRef = React.createRef();
  }
  
  
  // Checks if browser is compaitable to scan qr code.
  isNotCompaitable(){
   let userAgent = window.navigator.userAgent;
    let isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
      navigator.userAgent &&
      navigator.userAgent.indexOf('CriOS') === -1 &&
      navigator.userAgent.indexOf('FxiOS') === -1;
    
    // Return yes if safari
    if (isSafari) {
      return false;
    }

    // Return true iphone
    if ((userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) ) {
      return true;
    }
    else{

      var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
      if(!isChrome){
       return true;
      }
  
      return false;
    }
  
  }
  
  
  // Method to handle succesfull scan.
  handleQRScan(data){
    if(data) {
      let tableCode = data.substr(data.length - 4);
      if (tableCode.length === 4) {
        this.setState({redirect: true});
        this.setState({table_id: tableCode.toUpperCase()});
      }
      else {
        this.setState({error: true});
      }
    }
  }
  
  handleError(err){
    console.error(err)
  }
  
  
  // OnsubmitQRCode handler for Next button.
  submitQRCode() {
    var table_code = this.state.table_id || '';
    if(table_code.length === 4) {
      this.setState({redirect: true});
    }
    else{
      this.setState({error: true});
    }
  }
  
  // Key event handler to sync data between input component and scree.
  handleChangeValue = e => {
    var text = e.target.value;
    if(text.length===4) {
      this.setState({error: false});
      
    }
    this.setState({table_id: text.toUpperCase()});
    
  }
  
  // Open image dialog to scan QR in non supported browser.
   openImageDialog(){
     this.qrRef.current.openImageDialog();
  }
  
  render() {
    // Redirect to menu after valid table code
    if (this.state.redirect) {
      return <Redirect push to={"/menu/"+this.state.table_id} />;
    }
    var isCompatible = this.isNotCompaitable();
    var that = this;
    return (
      <div className="Table-code">
        <div className="Top-bar">
          <div className="Background">

          </div>
          <div className="Topbar-content">
            <div className="Logo-section">
              <div className="Sveet-logo">
                <img
                  src={require('../../assets/images/sveet-logo.svg')}
                  className="Sveet-logo-image"
                  alt="cafe logo"
                />
              </div>
              <div className="Logo-text">
                Order. Eat. Pay.
              </div>
            </div>
            <div className="QR-Section">
              <div className={isCompatible ? "QR-Scanner Qr-Click-Scanner" : "QR-Scanner"} >
                {isCompatible ? <div className="Qr-code" onClick={()=>{this.openImageDialog()}}>
                  <img
                    src={require('../../assets/images/icons/photo-camera.svg')}
                    className="Camera-icon"
                    alt="Camera icon"
                  />
                </div>: ''}
                <div className="Scanner-stick"></div>
                <QrReader
                  ref={this.qrRef}
                  delay={this.state.delay}
                  onError={this.handleError}
                  legacyMode={isCompatible ? true:false}
                  className="Qr-scanner-component"
                  onScan={this.handleQRScan}
                  style={{ width: '100%' }}
                />
              </div>
              <div className="QR-Scanner-text" >
                {isCompatible ? 'Click above to scan Qr Code' : 'Scan QR code'}
    
              </div>
            </div>
          </div>
        </div>
        <div className="Action-container">
          
          <div className="Or-text">OR</div>
          
          <div className={"Enter-OTP"+(this.state.error ? ' Error-Input' : '')}>
            <InputField
              type="text"
              placeholder="Enter your table code"
              value={this.state.table_id}
              onChangeValue={this.handleChangeValue}
            />
          </div>
          
          { this.state.error ?
            <Error
              text="Table id is not valid"
            />
            : ''
          }
          
          <div className="Next-button" >
            <Button
              align="center"
              color="Red"
              text="Next"
              type="simple"
              onClickEvent={that.submitQRCode.bind(this)}
              border
            />
          </div>
          
        </div>
      </div>
    );
  }
}

// Maps redux state data to props of this component
function mapStateToProps(state, props) {
  return { user_session: state.user_session };
}

// Maps redux dispatch method to props of this component
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ }, dispatch); // createSession()(dispatch);
}

// Connects this state and App.jsx redux calls and data passing
export default connect(mapStateToProps, mapDispatchToProps)(TableCode);
