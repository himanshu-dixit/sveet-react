import React, { Component } from 'react';
import './Menu.css';
import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import BottomBar from '../../components/menu/BottomBar/BottomBar';
import CategoryButton from '../../components/menu/CategoryButton/CategoryButton';
import CoverSlider from '../../components/menu/CoverSlider/CoverSlider';
import ImageMenutItem from '../../components/menu/ImageMenuItem/ImageMenuItem';
import GridMenuItem from '../../components/menu/GridMenuItem/GridMenuItem';
import DropdownMenu from '../../components/menu/DropdownMenu/DropbodownMenu';
import CustomizationBox from '../../components/menu/CustomizationBox/CustomizationBox';
import VegFilter from '../../components/standard/controls/VegFilter/VegFilter';
import Button from '../../components/standard/Button';
import Dialog from '../../components/standard/Dialog/Dialog';

import {
  createSession,
  addToCart,
  loginUser,
  createOrder,
  addItemToOrder,
  removeFromCart,
  updateVegFilter,
} from '../../actions/Index';

import {
  differentItem,
  findItemIDBYCount, itemCount, checkConnection, findItemByID, cartValue, getTotalNoItem,
} from '../../services/Menu.js';


class Menu extends Component {
  constructor(props) {
    super(props);
    // Get table code from urls

    this.state = {};
    // Load default session
  }

  componentDidMount() {
    const tableCode = this.props.match.params.table_code;
    // If User is logging in for the first time
    if (!this.props.store.table_code) {
      this.props.createSession(tableCode);
    }
    // If current session table code is different from other table code.
    else if (tableCode !== this.props.store.table_code.toUpperCase()) {
      this.props.createSession(tableCode, this.props.store.user_logged_in);
    }
  }

  render() {
    const that = this;

    // Variable to store data
    const storeInfo = this.props.store.store || '';
    const menu = this.props.menu || '';
    const cart = this.props.cart || '';
    const order = this.props.order || '';

    if (this.state.invalidTableCode) {
      return <Redirect push to="/" />;
    }

    if (this.state.redirect) {
      return <Redirect push to="/review/" />;
    }

    // Create list of categories for menu
    const categoriesList = ['Featured'];

    // Build category name list
    for (const k in this.props.menu.categories) {
      categoriesList.push(this.props.menu.categories[k].category_name);
    }

    // Set open state for customization box
    function openCustimzationBox(item) {
      that.setState({ customizationBoxVisible: 1, item_active: item });
    }

    // Used to add item to cart state by dispatching function
    function addItemToCart(itemId, customization) {
      const item = JSON.parse(JSON.stringify(findItemByID(menu, itemId)));
      // If no customization or customization has been done.
      if (item.item_customizations.length === 0 || customization) {
        // Add price of customization
        if (customization) {
          item.item_price += customization.total;
        }
        that.props.addToCart(item, customization || '');
        // Ensure customization box is closed
        that.setState({ customizationBoxVisible: 0, item_active: '' });
      }
      else {
        // show customization box
        openCustimzationBox(item);
      }
    }

    // Used to add item to cart state by dispatching function
    function removeItemfromCart(itemId) {
      const index = findItemIDBYCount(cart, itemId);
      const differentItemCount = differentItem(cart, itemId);
      console.log('test' + differentItemCount);
      if (differentItemCount > 1) {
        that.setState({ customizationError: 1 });
      }
      else {
        that.props.removeFromCart(index);
      }
    }

    function discard() {
      that.setState({ customizationBoxVisible: 0, item_active: '', customizationError: 0 });
    }

    // Place order
    function goBack() {
      that.setState({ invalidTableCode: true });
    }

    // Redirect to review order
    function reviewOrder() {
      that.setState({ redirect: true });
    }

    function updateVegFilter() {
      if (that.props.menu.is_veg_only === true) {
        this.props.updateVegFilter(false);
      }
      else {
        this.props.updateVegFilter(true);
      }
    }

    function dropdownItemCount(itemId) {
      return itemCount(cart, itemId);
    }

    // If there is invalid table code
    if (!menu.featured && this.props.store.table_code) {
      return (
        <div className="Error">
          <div className="Image-top">
            <img
              src={require('../../assets/images/icons/404.png')}
              width="100%"
              className="Error-404"
              alt="Error"
            />
            <div className="No-internet">
              Invalid Table Code
            </div>
          </div>
          <Button
            align="center"
            color="Red"
            text="Go Back"
            type="simple"
            onClickEvent={() => { goBack(); }}
            border
          />
        </div>
      );
    }

    // If user is not connected to internet
    if (this.props.store.clientstatus === false) {
      return (
        <div className="Error" onClick={() => { checkConnection(); }}>
          <div className="Image-top">
            <img
              src={require('../../assets/images/icons/no-internet.png')}
              width="100%"
              alt="Error"
              className="No-internet-icon"
            />
            <div className="No-internet">
            No Internet Connection
            </div>
          </div>
          <Button
            align="center"
            color="Red"
            text="Try Again"
            type="simple"
            onClickEvent={() => { checkConnection(); }}
            border
          />
        </div>
      );
    }

    // Loading Screen if there is no menu
    if (menu === '' || !menu.featured) {
      return (
        <div className="Loading">
          <div className="Image-top">
            <img
              src={require('../../assets/images/icons/loader.svg')}
              className="Loader-image"
              alt="Loading"
            />
            <div className="No-internet">
              Loading
            </div>
          </div>

        </div>
      );
    }


    return (
      <div className="Menu">
        <div className="Top-bar">
          <div className="Background">

          </div>
          <div className="Background-overlay">
          </div>
          <div className="Topbar-content">
            <div className="Cafe-logo">
              {storeInfo.cafe_name}
            </div>

          </div>
        </div>
        <div className="Custimzation-box">

          {this.state.customizationBoxVisible === 1
            ? (
              <CustomizationBox
                OnDiscard={discard.bind(this)}
                OnClick={addItemToCart.bind(this)}
                customization_params={this.state.item_active}
              />
            ) : ''}

          {this.state.customizationError === 1
            ? (
              <Dialog
                OnDiscard={discard.bind(this)}
              />
            ) : ''}

        </div>

        <div className="Round-container">
          <div>

            <CoverSlider image_url={storeInfo.featured ? storeInfo.featured : ''} />

            <div className="Initial-box">
              <span className="Featured-menu" id="Featured">Featured</span>
              <div className="Veg-filter-box">
                <span className="Veg-text">Veg Only</span>
                <VegFilter
                  currentState={that.props.menu.is_veg_only}
                  OnClick={updateVegFilter.bind(this)}
                />
              </div>
            </div>

            {/* Featured items */}
            <div className="Featured-container">
              {
                menu.featured.map(function (item) {
                  if (((that.props.menu.is_veg_only === item.is_veg
                    && item.is_veg === false)
                    || item.is_veg === true)) {
                    return (
                      <GridMenuItem
                        store_id={storeInfo.cafe_id}
                        key={item.item_id}
                        item_id={item.item_id}
                        name={item.item_name}
                        item_count={itemCount(cart, item.item_id)}
                        OnDelete={removeItemfromCart.bind(this)}
                        OnClick={addItemToCart.bind(this)}
                        isVeg={item.is_veg}
                        price={item.item_price}
                        description={item.item_desc}
                        image_url={item.item_pic}
                      />
                    );
                  }
                })
              }
            </div>


            {/* Iterate over sections */}

            {Object.keys(menu.categories).map((key) => {
              const sectionData = menu.categories[key].sub_categories;
              let elementID = menu.categories[key].category_name.replace(/\s+/g, '-');
              elementID = elementID.replace(/[^\w\s]/gi, '');
              // Iterate over categories in sections

              return (
                <div>
                  <div id={elementID} className="Section-heading">{menu.categories[key].category_name}</div>
                  <div>
                    {
                      Object.keys(sectionData).map(function (key) {
                        const subCategoryName = sectionData[key].sub_category_name;
                        if (!(subCategoryName === '__direct')) {
                          return (
                            <DropdownMenu
                              category_name={subCategoryName}
                              showList={0}
                              item_count={dropdownItemCount.bind(this)}
                              OnClick={addItemToCart.bind(this)}
                              OnDelete={removeItemfromCart.bind(this)}
                              isVegOnly={that.props.menu.is_veg_only}
                              key={subCategoryName}
                              data={sectionData[key].items}
                            />
                          );
                        }

                        return (
                          sectionData[key].items.map(function (sectionItem) {
                            if (((that.props.menu.is_veg_only === sectionItem.is_veg
                              && sectionItem.is_veg === false) || sectionItem.is_veg === true)) {
                              return (
                                <ImageMenutItem
                                  store_id={storeInfo.cafe_id}
                                  key={sectionItem.item_id}
                                  item_id={sectionItem.item_id}
                                  name={sectionItem.item_name}
                                  item_count={itemCount(cart, sectionItem.item_id)}
                                  OnClick={addItemToCart.bind(this)}
                                  OnDelete={removeItemfromCart.bind(this)}
                                  isVeg={sectionItem.is_veg}
                                  price={sectionItem.item_price}
                                  description={sectionItem.item_desc}
                                  image_url={sectionItem.item_pic}
                                />
                              );
                            }
                          })
                        );
                      })
                    }
                  </div>
                </div>
              );
            })
            }

          </div>
        </div>

        <div className="Category-float-button">
          <CategoryButton categories_list={categoriesList} />
        </div>

        {order.length + cart.length > 0
          ? (
            <div className="Bottom-float-button">
              <BottomBar
                color="Blue"
                item={getTotalNoItem(cart, order)}
                amount={cartValue(cart, order)}
                OnClick={reviewOrder}
                next_icon="yes"
                icon_class="fas fa-utensils"
              />
            </div>
          ) : ''}


      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    store: state.user_session, menu: state.menu, cart: state.cart, order: state.order,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createSession,
    addToCart,
    loginUser,
    createOrder,
    addItemToOrder,
    updateVegFilter,
    removeFromCart,
  }, dispatch); // createSession()(dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
