import React, { Component } from 'react';
import './OrderInfo.css';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router } from 'react-router-dom';
import Button from '../../components/standard/Button';
import Pay from '../../components/standard/Pay/Pay';
import Complete from '../../components/standard/Complete/Complete';
import {
  addItemToOrder, createOrder, createSession, loginUser, removeFromCart, updateCart,
} from '../../actions/Index';
import { cartValue, checkConnection } from '../../services/Menu';

class OrderInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false, redirectToMenu: false, showPay: 0, showComplete: 0, showWater: 0, paymentType: 'cash',
    };
    window.jQuery([document.documentElement, document.body]).animate({
      scrollTop: 0,
    }, 10);
  }

  click() {
    this.setState({ redirect: true });
  }

  redirectToMenu() {
    if (!this.state.redirectToMenu) this.setState({ redirectToMenu: true });
  }

  togglePay() {
    if (this.state.showPay === 1) {
      this.setState({ showPay: 0 });
    }
    else {
      this.setState({ showPay: 1 });
    }
  }

  toggleComplete(type) {
    if (type === 'credit') {
      this.setState({ paymentType: 'credit' });
    }
    this.setState({ showPay: 0 });
    this.setState({ showComplete: 1 });
    // Should make changes to user session
  }

  toggleWater() {
    this.setState({ showWater: 1 });
  }


  render() {

    let amount = 0;
    const that = this;
    const order = this.props.order;
    const cart = this.props.cart;
    const table_code = this.props.store.table_code;
    const order_id = Number(this.props.store.order_id);
    const user_id = Number(this.props.customer.user_id);
    this.props.order.forEach((order) => {
      if (order) {
        amount += order.item.item_price;
      }
    });


    const storeInfo = this.props.store.store;
    if (this.state.redirect) {
      return <Router><Redirect push to="/payments/" /></Router>;
    }
    if (this.state.redirectToMenu) {
      return <Redirect push to="/menu/ABCD" />;
    }

    function discard() {
      that.setState({ showPay: 0 });
    }

    function floorFigure(figure, decimals) {
      if (!decimals) decimals = 2;
      const d = Math.pow(10, decimals);
      return (parseInt(figure * d) / d).toFixed(decimals);
    }
  
  
    // If user is not connected to internet
    if (this.props.store.clientstatus === false) {
      return (
        <div className="Error" onClick={() => { checkConnection(); }}>
          <div className="Image-top">
            <img
              src={require('../../assets/images/icons/no-internet.png')}
              width="100%"
              alt="Error"
              className="No-internet-icon"
            />
            <div className="No-internet">
              No Internet Connection
            </div>
          </div>
          <Button
            align="center"
            color="Red"
            text="Try Again"
            type="simple"
            onClickEvent={() => { checkConnection()}}
            border
          />
        </div>
      );
    }

    return (
      <div className="Order-info-screen">
        <div className="Top-bar">
          <div className="Background">
          </div>

          <div className="Topbar-content">
            <div className="Cafe-logo">
              {storeInfo.cafe_name}
            </div>
          </div>
        </div>


        <div className="Round-container">
          <div>
            {/* Iterate over sections */}

            <div className="Order-Container">

              <div className="Inner-Container">

                <div className="Order-confirmation">
                  <div className="Preparing-icon">
                    <img
                      src={require('../../assets/images/icons/burger_loading.gif')}
                      width="100%"
                      alt="Preparing"
                      className="Preparing-icon-image"
                    />
                  </div>
                  We have received your order
                </div>
                

                <div className="Action-container">

                  {/* <Button */}
                  {/* align="center" */}
                  {/* color="Red" */}
                  {/* text="Request Water" */}
                  {/* type="simple" */}
                  {/* onClickEvent={() => { this.toggleWater(); }} */}
                  {/* border */}
                  {/* /> */}
                  {/* {this.state.showWater ? ( */}
                  {/* <span clasName="water"> */}
                  {/* <br /> */}
                  {/* We're bringing your water */}
                  {/* <br /> */}
                  {/* </span> */}
                  {/* ) : ''} */}
                  <Button
                    align="center"
                    color="Blue"
                    text="Order More"
                    onClickEvent={() => { this.redirectToMenu(); }}
                    type="simple"
                    border
                  />

                </div>
                <div className="Item-total-bar">

                  <div className="Total">
                    Item Total
                  </div>

                  <span className="Total-price">
                  ₹
                    {' '}
                    {cartValue(cart, order)}
                  </span>
                </div>
                <div className="Tax-total-bar">

                  <div className="Total">
                    CGST - 2.5%
                  </div>

                  <span className="Total-price">
                   ₹
                    {' '}
                    {floorFigure((cartValue(cart, order) * 0.025))}
                  </span>
                </div>
                <div className="Tax-total-bar">

                  <div className="Total">
                    SGST - 2.5%
                  </div>

                  <span className="Total-price">
                    ₹
                    {' '}
                    {floorFigure(cartValue(cart, order) * 0.025)}
                  </span>
                </div>
                <div className="Total-bar">

                  <div className="Total">
                    Total
                  </div>

                  <span className="Total-price">
                  ₹
                    {' '}
                    {floorFigure(cartValue(cart, order) * 1.05)}
                  </span>
                </div>

                <Button
                  align="center"
                  color="Red"
                  text="Pay for my order"
                  type="simple"
                  onClickEvent={this.togglePay.bind(this)}
                  border
                />
              </div>
            </div>

            {this.state.showPay ? (
              <Pay
                OnDiscard={discard.bind(this)}
                order_id={order_id}
                table_code={table_code}
                user_id={user_id}
                complete={this.toggleComplete.bind(this)}
                amount={floorFigure(cartValue(cart, order) * 1.05)}
              />
            ) : ''}

            {this.state.showComplete ? (
              <Complete
                type={this.state.paymentType}
                token={this.props.customer.token}
                order_id={order_id}
                table_code={table_code}
                user_id={user_id}
                amount={floorFigure(cartValue(cart, order) * 1.05)}
              />
            ) : ''}
          </div>

        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    store: state.user_session,
    menu: state.menu,
    cart: state.cart,
    order: state.order,
    customer: state.customer,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createSession, updateCart, loginUser, createOrder, addItemToOrder, removeFromCart,
  }, dispatch); // createSession()(dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderInfo);
