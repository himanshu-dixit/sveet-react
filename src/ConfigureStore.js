import { createStore, applyMiddleware } from 'redux';
// Logger with default options
import logger from 'redux-logger';
import rootReducer from './reducers/Index.js';

export default function configureStore() {
  return createStore(
    rootReducer,
    applyMiddleware(logger),
  );
}
