/**
 * Foldable Dropdown menu container
 * Props - category_name [String], item_list [Array]
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './DropbodownMenu.css';
import PropTypes from 'prop-types';
import SimpleMenuItem from '../../standard/SimpleMenuItem/SimpleMenuItem';

class DropdownMenu extends Component {
  constructor(props) {
    super(props);
    this.state = { showList: this.props.showList };
  }

  OnClick() {
    if (this.state.showList) {
      this.setState({ showList: 0 });
    }
    else {
      this.setState({ showList: 1 });
    }
  }

  render() {
    const that = this;
    const data = this.props.data || '';
    return (
      <div className="Dropdown-menu">
        <div className="Dropdown-heading-box" onClick={this.OnClick.bind(this)}>
          <span className="Dropdown-heading-text">
            {this.props.category_name}
          </span>
          <span className="Dropdown-heading-icon">
            <i className="fas fa-chevron-down"></i>
          </span>
        </div>
        {this.state.showList === 1
        && (
        <div className="Dropdown-menu-list">
          {/* Iterate over category data items */}
          {
            data.map((item) => {
              if (
                (
                  (that.props.isVegOnly === item.is_veg && item.is_veg === false)
                ) || item.is_veg === true) {
                console.log(item);
                return (
                  <SimpleMenuItem
                    key={Math.random() + item.item_name}
                    isVeg={item.is_veg}
                    name={item.item_name}
                    price={item.item_price}
                    item_count={that.props.item_count(item.item_id)}
                    item_id={item.item_id}
                    OnClick={that.props.OnClick}
                    OnDelete={that.props.OnDelete}
                    description={item.item_desc}
                  />
                );
              }
              return 1;
            })
          }
        </div>
        )
        }
      </div>
    );
  }
}

DropdownMenu.propTypes = {
  category_name: PropTypes.string.isRequired,
  data: PropTypes.string,
  showList: PropTypes.number,
};

export default DropdownMenu;
