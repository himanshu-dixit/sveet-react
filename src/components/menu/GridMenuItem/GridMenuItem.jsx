/**
 * Grid menu item
 * Props - image_url[String], name[String], description[String], price[Number]
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './GridMenuItem.css';
import PropTypes from 'prop-types';
import AddItemButton from '../../standard/AddItemButton/AddItemButton';
import ImageLoader from 'react-load-image';

class GridMenuItem extends Component {
  render() {

    return (
      <div className="Grid-Card">
        <div className="Image-menu-card">
          <div className="Image-box" >
            {/*First child on succsfull, 2nd on failure and 3rd on processing*/}
            <ImageLoader
              src={this.props.image_url}
              width="100%"
            >
              <img width="100%"/>
              <img width="100%"/>
              <img src={require('../../../assets/images/icons/loading.gif')} width="100%"/>
            </ImageLoader>
          </div>
          <div className="Menu-details">

            <div className="Item-meta">
              {
                this.props.isVeg === true ? (
                  <img
                    src="https://i.imgur.com/kVTVaxd.png"
                    height="10px"
                    width="10px"
                    alt="veg"
                  />
                ) : (
                  <img
                    src="https://i.imgur.com/86tZP2e.png"
                    height="10px"
                    width="10px"
                    alt="veg"
                  />
                )
            }
            </div>
            <div className="Item-name">
              {this.props.name}
            </div>

            <div className="Menu-bottom-bar">
              <div className="Item-price">
              ₹
                {' '}
                {this.props.price}
              </div>
              <div className="Add-item-button">
                <AddItemButton
                  OnDelete={() => { this.props.OnDelete(this.props.item_id); }}
                  OnClick={() => { this.props.OnClick(this.props.item_id); }}
                  item_count={this.props.item_count}
                />
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

GridMenuItem.propTypes = {
  image_url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  isVeg: PropTypes.bool.isRequired,
  item_count: PropTypes.number,
  item_id: PropTypes.number.isRequired,
};

export default GridMenuItem;
