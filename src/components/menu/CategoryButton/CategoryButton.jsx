/**
 * Category foldable button
 * Props available - color{blue}
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './CategoryButton.css';
import PropTypes from 'prop-types';
window.$ = window.jQuery = require('jquery')


class CategoryButton extends Component {
  static defaultProps = {
    color: ''
  }
  
  constructor() {
    super();
    this.state = {showMenu: 0, selectedCategory: ""};
  }
  
  OnClick() {
    if (this.state.showMenu) {
      this.setState({showMenu: 0});
      window.jQuery('html, body').css({
        overflow: 'auto',
        height: 'auto'
      });
    }
    else {
      window.jQuery('html, body').css({
        overflow: 'hidden',
        height: '100%'
      });
      this.setState({showMenu: 1});
    }
  }
 
  SelectCategory(category) {
    this.setState({selectedCategory: category});
    this.OnClick();
    
    
    // Scroll
    let text = category.category;
    let element_id = text.replace(/\s+/g, '-');
    element_id = element_id.replace(/[^\w\s]/gi, '');
    window.jQuery([document.documentElement, document.body]).animate({
      scrollTop: (window.jQuery("#"+element_id).offset().top)-100
    }, 200);
    
    let flash_count = 0;
    
    let refreshIntervalId = setInterval(function () {
      
      // Increment Flash count
      flash_count++;
      
      // Clear set interval after 2 flash
      if(flash_count>1){
        clearInterval(refreshIntervalId);
      }
      
      // Dispatch javascript function call to flash element
      window.jQuery('#'+element_id).fadeOut(400);
      window.jQuery('#'+element_id).fadeIn(400);
    }, 200);
    

    // Perform actions
  }
  
  render() {
    
    const categories_list = this.props.categories_list;
    let className = 'Menu-button Button-right Border';
    let that = this;
    if (this.props.color) {
      className += " " + this.props.color;
    }
    
    return (
      <div>
        {this.state.showMenu === 1 ?
          <div className="Category-overlay">
            <div className="Overflow-content"></div>
          </div>
          : ''}
        {this.state.showMenu === 1 &&
        <div className="Category-box">
          <ul>
            {
              categories_list.map(function(category){
                
                return <li className={
                  (that.state.selectedCategory.category === category ?
                    'Selected' : 'new')}
                           onClick={
                             () => that.SelectCategory({category})
                           }
                  key={category}
                >{category}</li>
              })
            }
          
          </ul>
        </div>
        }
        <div
          className={className}
          tabIndex={1}
          onClick={this.OnClick.bind(this)}>
          
          <i
            className="fas fa-utensils Menu-button-icon"
          ></i>
          Menu
        </div>
      </div>
    );
  }
}

CategoryButton.propTypes = {
  color: PropTypes.string,
}

export default CategoryButton;
