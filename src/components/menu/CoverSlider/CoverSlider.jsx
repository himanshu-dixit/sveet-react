/**
 * Featured cover slider for menu screen
 * Props available - image_url [string]
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './CoverSlider.css';
import PropTypes from 'prop-types';
import ImageLoader from 'react-load-image';

class CoverSlider extends Component {
  render() {
    function Preloader(props) {
      return <img src={require('../../../assets/images/icons/loading.gif')} />;
    }
    return (
      <div
        className="Cover-slider"
      >
        {/* First child on succsfull, 2nd on failure and 3rd on processing */}
        <ImageLoader
          src={this.props.image_url}
          width="100%"
        >
          <img width="100%" />
          <img width="100%" />
          <img src={require('../../../assets/images/icons/loading.gif')} width="100%" />
        </ImageLoader>
      </div>
    );
  }
}

CoverSlider.propTypes = {
  image_url: PropTypes.string.isRequired,
};

export default CoverSlider;
