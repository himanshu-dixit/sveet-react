/**
 * Item Customization box.
 * Props available - color
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './CustomizationBox.css';
import PropTypes from 'prop-types';
import InputField from '../../standard/InputField';
import Button from '../../standard/Button';

class CustomizationBox extends Component {
  
  constructor(){
    super();
    this.state = {remarks: ''};
  }
  
  handleChangeValue = e => {
    var text = e.target.value;
    this.setState({remarks: text, amount: 0});
  }
  
  render() {
    const item = this.props.customization_params;
    const item_id = item.item_id;
    
    let selectedValue = {
      selectedCustomization: [],
      remarks: this.state.remarks,
      total: 0
    }
    
    function changeCustomization(custom_type,value,amount,length){
      let record = {"name": custom_type,"value": value,"amount": amount, select: length};
      if(!selectedValue.selectedCustomization[custom_type]){
        selectedValue.selectedCustomization[custom_type] = [];
      }
      // If multiple addon are allowed
      if(length==='multiple'){
        let entryExist = 0;
        selectedValue.selectedCustomization[custom_type].forEach((value)=>{
          // Do deep comparison
          if(value.value===record.value &&  value.amount===record.amount && entryExist === 0){
            entryExist = 1
          }
        })
        if(entryExist){
          return
        }
        else {
          selectedValue.selectedCustomization[custom_type].push(record);
          computeValue();
        }
      }
      else{
        selectedValue.selectedCustomization[custom_type] = record;
        computeValue();
      }
      
    }
    function computeValue() {
      let amount = 0;
      Object.keys(selectedValue.selectedCustomization).map(function (key) {
        let customizationRecord = selectedValue.selectedCustomization[key];
        if(customizationRecord instanceof Array){
          customizationRecord.forEach((customization)=>{
            amount = amount+ customization.amount;
          })
        }
        else{
          amount = amount+customizationRecord.amount;
        }
        return amount;
      })
      selectedValue.total = amount;
      // Put translation service here
      return;
    }
    
    return <div className="Overlay">
      <div className="Overlay-box">
        
        <div className="Overlay-heading">Customize
          <div className="Close"
               onClick={() => {this.props.OnDiscard()}}>✕</div>
        </div>
        <div className="Customization-container">
          {Object.keys(item.item_customizations).map(function (key) {
            let data = item.item_customizations[key];
            if(data.select === 'single'){
              return (
                <div key={data.name}>
                  <div className="Customization-name"> {data.name} </div>
                  <div className="Customization-control">
                    {
                      Object.keys(data.customization).map(function (key) {
                        
                        return (
                          <div
                            className="Radio-button"
                            key={data.customization[key].customization_name}
                            onClick={() => {
                              changeCustomization(data.name,
                                data.customization[key].customization_name,
                                data.customization[key].customization_additional_price,
                                'single')
                            }}>
                            <label className="">
                              <input type="radio" name={data.name} value={data.customization[key].customization_name}/ >
                              <span>{data.customization[key].customization_name} - ₹ {data.customization[key].customization_additional_price} </span>
                            </label>
                          </div>
                      )
                      })
                      }
                  </div>
                </div>
              )
            }
            else if(data.select === 'multiple'){
              return (
                <div key={data.name}>
                  <div className="Customization-name"> {data.name} </div>
                  <div className="Customization-control">
                    
                    {
                      Object.keys(data.customization).map(function (key) {
              
                        return (
                          <li    key={data.customization[key].customization_name}
                                 onClick={() => {
                                   changeCustomization(data.name,
                                     data.customization[key].customization_name,
                                     data.customization[key].customization_additional_price,
                                     'multiple')
                                 }}>
                          <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value={data.customization[key].customization_name} />
                            <label htmlFor="styled-checkbox-1" className="Checkbox-label">{data.customization[key].customization_name} - ₹ {data.customization[key].customization_additional_price}</label>
                          </li>
                      )
                      })
                      }
                  </div>
                </div>
              )
            }
            
          })}
        
        </div>
        
        <div className="Additional-remarks">
          Additional remarks
        </div>
        <div className="Customization-remarks">
          <InputField
            type="text"
            placeholder=""
            value={this.state.remarks}
            onChangeValue={this.handleChangeValue}
          />
        </div>
        
        <div className="Customization-add-button">
          <Button
            align="center"
            color="Red"
            text="Add Item"
            type="simple"
            param1={item_id}
            param2={selectedValue}
            onClickEvent={this.props.OnClick}
            border
          />
        </div>
      </div>
    </div>;
  }
}

CustomizationBox.propTypes = {
  customization_params: PropTypes.object.isRequired,
}

export default CustomizationBox;
