/**
 * Clickable and customizable menu button
 * Props available - color{blue}, item[integer], amount[integer], next_icon[boolean],
 * icon_class [string]
 *  icon_button}
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './BottomBar.css';
import PropTypes from 'prop-types';


class BottomBar extends Component {
  static defaultProps = {
    item: 0,
    amount: 0,
    color: 'Grey',
    text: 'Place order',
    next_icon: 'no',
    icon_class: 'fab fa-twitter'
  }
  render(){
    let className = 'Next-Button  Button-right Border';
    let iconClass = "Button-icon "+this.props.icon_class;
    
      return (
        <div
          className={className}
          onClick={()=>{this.props.OnClick()}}
          >
          <div className="First-section">
          <i className={iconClass}></i>
            <span className="Price">
            {this.props.item > 0
            && (
              this.props.item+ " Items | ₹ "+this.props.amount
            )
            }
            </span>
        </div>
          <div className="Second-section">
            {this.props.text}
            {this.props.next_icon === "yes"
            && (
              <i className="fas fa-arrow-right"></i>
            )
            }
          </div>
        </div>
      );
  }
}

BottomBar.propTypes = {
  color: PropTypes.string,
  item: PropTypes.number,
  amount: PropTypes.number,
  text: PropTypes.string,
  icon_class: PropTypes.string,
  next_icon: PropTypes.string
}

export default BottomBar;
