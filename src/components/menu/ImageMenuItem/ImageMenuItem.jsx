/**
 * Image menu item
 * Props - image_url[String], name[String], description[String], price[Number]
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './ImageMenuItem.css';
import PropTypes from 'prop-types';
import AddItemButton from '../../standard/AddItemButton/AddItemButton';


class ImageMenuItem extends Component {
  render() {
    return (
      <div className="Image-menu-card-list">
        { this.props.image_url ?
        <div className="Image-box" style={{ backgroundImage: 'url("' + this.props.image_url + '")' }}>
        </div>
          : <div className="Whitespace-box"></div> }
        <div className="Menu-details">

          <div className="Item-meta">
            {
              this.props.isVeg? (
                <img
                  src="https://i.imgur.com/kVTVaxd.png"
                  height="13px"
                  width="13px"
                  alt="veg"
                />
              ) : (
                <img
                  src="https://i.imgur.com/86tZP2e.png"
                  height="13px"
                  width="13px"
                  alt="veg"
                />
              )
            }
          </div>
          <div className="Item-name">
            {this.props.name}
          </div>

          <div className="Item-description">
            {this.props.description.length > 80 ? this.props.description.substr(0, 80) +'...' : this.props.description}
          </div>

          <div className="Menu-bottom-bar">
            <div className="Item-price">
              ₹
              {' '}
              {this.props.price}
            </div>
            <div className="Add-item-button">
              {this.props.show_button === "no" ?
              <AddItemButton
                OnClick={() => { this.props.OnClick(this.props.item_id); }}
                OnDelete={ this.props.OnDelete(this.props.item_id).bind(this)}
                item_count={this.props.item_count}
              />
                :
                <AddItemButton
                  OnClick={() => { this.props.OnClick(this.props.item_id); }}
                  OnDelete={() => { this.props.OnDelete(this.props.item_id); }}
                  item_count={this.props.item_count}
                />}

            </div>
          </div>

        </div>
      </div>
    );
  }
}

ImageMenuItem.propTypes = {
  item_id: PropTypes.number.isRequired,
  image_url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  isVeg: PropTypes.bool.isRequired,
  item_count: PropTypes.number,
};

export default ImageMenuItem;
