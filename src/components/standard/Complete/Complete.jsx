/**
 * OTP verification Box
 * Props available - color
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './Complete.css';
import Button from '../Button';
import InputField from '../InputField';
import {sendRequest} from "../../../services/APICall";

class Complete extends Component {
  constructor(props){
    super(props);
    this.state = {
      rating: -1
    };
  }
  handleChangeValue = e => {
    var text = e.target.value;
    if(text.length===4) {
      this.setState({error: false});
      
    }
    this.setState({remarks: text});
    
  }
  submitFeedback(){

    if(this.state.rating > 0) {
      
      const requestJSON = {
        order_id: this.props.order_id,
        user_id: this.props.user_id,
        table_code: this.props.table_code,
        rating: this.state.rating,
        remarks: this.state.remarks,
      };

      return sendRequest('submitFeedback', requestJSON, this.props.token).then((data)=>{
        if(data.status=="success") {
          this.setState({feedbackReceived: true});
          this.setState({error: false});
        }
      });

    }
    else{
      this.setState({error: true});
    }
  }
  render() {
    let rating = this.state.rating;
    let that = this;
    function setRating(number){
      that.setState({rating: number});
    }
    return (
      <div className="Overlay">
        <div className="Overlay-box">
          <div>
            <div className="Overlay-heading">Thank your for using Sveet</div>

            <div className="OTP-container">
              { this.props.type === 'credit'
                ? <div className="Tick-mark" /> : (
                  <div>
                    <p className="Thanks-message">
                      Please pay cash to the waiter
                    </p>
                    <div className="Pay-cash"></div>
                  </div>
                )}
  
              <p className="Feedback-message">
                How was your experience at Big Yellow Door?
              </p>
  
              <div className="Feedback">
                <i className={rating >=1 && rating > 0 ? "fas fa-star selected" : "fas fa-star"} onClick={()=>{setRating(1);}}></i>
                <i className={rating >=2 && rating > 0 ? "fas fa-star selected" : "fas fa-star"} onClick={()=>{setRating(2);}}></i>
                <i className={rating >=3 && rating > 0 ? "fas fa-star selected" : "fas fa-star"} onClick={()=>{setRating(3);}}></i>
                <i className={rating >=4 && rating > 0 ? "fas fa-star selected" : "fas fa-star"} onClick={()=>{setRating(4);}}></i>
                <i className={rating >=5 && rating > 0 ? "fas fa-star selected" : "fas fa-star"} onClick={()=>{setRating(5);}}></i>
              </div>
  
              <InputField
                type="text"
                placeholder="Enter your remarks"
                value={this.state.remarks}
                onChangeValue={this.handleChangeValue}
              />
              <div className="Next-button" >
                <Button
                  align="center"
                  color="Blue"
                  text="Submit"
                  type="simple"
                  onClickEvent={that.submitFeedback.bind(this)}
                  border
                />
              </div>
              {this.state.error? <p>Please add rating</p> : ''}
              {this.state.feedbackReceived? <p>We have received your feedback</p> : ''}
              {/*<div className="OTP-add-button">*/}
                {/*<Button*/}
                  {/*align="center"*/}
                  {/*color="Blue"*/}
                  {/*text="Share with your Friends"*/}
                  {/*type="simple"*/}
                  {/*border*/}
                {/*/>*/}
              {/*</div>*/}

            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Complete;
