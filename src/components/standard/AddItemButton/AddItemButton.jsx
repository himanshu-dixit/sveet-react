/**
 * Add item to cart container.
 * Props available - color{blue}, item_id[number]
 * @version 1.0.0
 * Date Created - 18/08/2018
 */

import React, { Component } from 'react';
import './AddItemButton.css';
import PropTypes from 'prop-types';


class AddItemButton extends Component {
  static defaultProps = {
    color: '',
    item_id: 0
  }
  render(){
    let that = this;
    let className = "Add-button";
    if (this.props.color) {
      className += " "+this.props.color;
    }
    
    return (
      <div>

        {this.props.item_count > 0 ?
  
          <div className={className+' Item-box'} >
            <div className="Minus-box"
                 onClick={
                   () => {this.props.OnDelete(this.props.item_id)
                   }}>
              <i className="fas fa-minus"></i>
            </div>
            <div className="Item-count-number">
              {this.props.item_count}
            </div>
            <div className="Plus-box" onClick={
              () => {this.props.OnClick(this.props.item_id)
              }}>
              <i className="fas fa-plus"></i>
            </div>
          </div>
          :
          <div className={className} onClick={
            () => {this.props.OnClick(this.props.item_id)
            }}>
            <div className="Add-box">
              ADD
            </div>
            <div className="Add-box-button">
              <i className="fas fa-plus"></i>
   
            </div>
          </div>
        }
      </div>
    );
  }
}

AddItemButton.propTypes = {
  item_id: PropTypes.number,
  color: PropTypes.string,
}

export default AddItemButton;
