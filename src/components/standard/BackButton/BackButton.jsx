import React, { Component } from 'react';
import './BackButton.css';
import { Redirect } from 'react-router';

class BackButton extends Component {
  constructor(props) {
    super(props);
    this.state = { redirect: false};
  }

  click() {
    this.setState({redirect: true});
  }

  render() {
    if (this.state.redirect) {
      return <Redirect push to={this.props.screen} />;
    }
    return (
      <div>
        <div className="BackButton" onClick={() => { this.click() }}>
          <i className="fas fa-chevron-left"></i>
        </div>
      </div>
    );
  }
}

export default BackButton;
