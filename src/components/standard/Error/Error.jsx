/**
 * Component to display error
 * Props available - text[String]
 * @version 1.0.0
 * Date Created - 30/08/2018
 */

import React, { Component } from 'react';
import './Error.css';
import PropTypes from 'prop-types';

class Error extends Component {
  render() {
    return (
      <div className="Error-dialog">
        {this.props.text}
      </div>
    );
  }
}

Error.propTypes = {
  text: PropTypes.string.isRequired,
}

export default Error;
