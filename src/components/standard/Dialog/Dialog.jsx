/**
 * OTP verification Box
 * Props available - color
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './Dialog.css';
import InputField from '../InputField';
import Button from '../Button';
class Dialog extends Component {
  render() {
    const that = this;
    return (
      <div className="Overlay">
        <div className="Overlay-box">
          <div>
            <div className="Overlay-heading">
              Oops
              <div className="Close" onClick={() => { this.props.OnDiscard(); }}>✕</div>
              {' '}

            </div>
            <div className="OTP-container">
              <div className="Dialog-remarks">
                This item has different customizations, Please change it from review page.
              </div>
              <div className="OTP-add-button">
                <Button
                  align="center"
                  color="Blue"
                  text="Go Back"
                  type="simple"
                  onClickEvent={() => { this.props.OnDiscard(); }}
                  border
                />

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dialog;
