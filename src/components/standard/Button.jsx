/**
 * Clickable and customizable standard button
 * Props available - color{blue}, border[bool], align{center}, type{simple,
 *  icon_button}
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Button.css';

class Button extends Component {
  constructor(props){
    super(props);
    this.state = {timeOut: 0};
  }

  static defaultProps = {
    type: 'simple',
    color: 'Red'
  }
  render(){
    let that = this;
    let className = 'Button';
    
    if (this.props.color) {
      className += " "+this.props.color;
    }
    
    if (this.props.border) {
      className += ' Border';
    }
    
    if (this.props.align) {
      className += ' Center';
    }
    
    function makeButtonInactive() {
      that.setState({timeOut: 5})
      clearInterval(refreshIntervalId);
      // Start the counter
      let refreshIntervalId = setInterval(function () {
        that.setState({timeOut: that.state.timeOut-1});
        if(that.state.timeOut < 1){
          clearInterval(refreshIntervalId);
        }
      }, 1000);
    }
    
    // Button with icon
    if(this.props.type === "simple") {
      
      return (
        <button
          className={that.state.timeOut> 0 ? className+" Button-Inactive" :className}
          onClick={() => {
              this.props.onClickEvent(this.props.param1,this.props.param2)
              makeButtonInactive();
            }
          }
          disabled={that.state.timeOut> 0? 1: 0}
        >
          {that.state.timeOut> 0?
          <div className="Button-Loading">
            <img
              src={require('../../assets/images/icons/spinner.svg')}
              className="spinner"
              alt="cafe logo"
            />
          </div> : ''}
          {this.props.text}
          <span className="Arrow-right">
              <img
                src={require('../../assets/images/icons/next.svg')}
                className="Next-icon"
                alt="cafe logo"
              />
          </span>
        </button>
      );
      
    }
    else{
      
      className += ' Button-right';
      return (
        <button
          onClick={() => {this.props.onClickEvent(this.props.param1,this.props.param2)}}
          className={className}>
          <i
            className="fab fa-facebook Button-icon"
          ></i>
            {this.props.text}
          </button>
      );
      
    }
  }
}

Button.propTypes = {
  color: PropTypes.string.isRequired,
  border: PropTypes.bool.isRequired,
  align: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  type: PropTypes.string
}

export default Button;
