/**
 * OTP verification Box
 * Props available - color
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './Pay.css';
import Button from '../Button';
import { completeOrder } from '../../../actions/Index.js';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";

class Pay extends Component {
  constructor(props) {
    super(props);
    this.state = { OTPSent: 0 };
    console.log(this.props.store.order_id);
  }

  startPaymentByCash() {
    // Show next screen. Update transaction Status
    this.props.completeOrder('','cash', this.props.store.order_id, this.props.customer.token);
    this.props.complete('cash');
  }

  razorPayCallback(response) {
    let transaction_id = response.razorpay_payment_id;
    this.props.completeOrder(transaction_id,'credit', this.props.store.order_id, this.props.customer.token);
    // Should update the transaction and order
    this.props.complete('credit');
  }

  startPaymentByCredit() {

    const that = this;
    // Should create new transaction
    // Show next screen. Update transaction Status
    const options = {
      key: 'rzp_test_U9Cs8cFhv6y9Ub',
      amount: that.props.amount * 100, // 2000 paise = INR 20
      name: 'Sveet',
      description: 'Dine in at Big Yellow Door',
      image: '/your_logo.png',
      handler(response) {
        that.razorPayCallback(response);
      },
      prefill: {
        name: ' ',
        email: that.props.customer.phone+'@sveet.io',
        contact: that.props.customer.phone,
      },
      notes: {
        address: '',
      },
      theme: {
        color: '#4084ff',
      },
      external: {
        wallets: ['paytm',],
        handler: function(response) {
          // write code on how to handle the external wallet here, based on the param 'data'
          that.razorPayCallback(response);
        }
      },
    };
    const rzp1 = new window.Razorpay(options);
    rzp1.open();
  }

  render() {
    return (
      <div className="Overlay">
        <div className="Overlay-box">

          <div>
            <div className="Overlay-heading">
              Pay for your order
              <div className="Close" onClick={() => { this.props.OnDiscard(); }}>
                ✕
              </div>
            </div>
            <div className="OTP-container">
              <p className="Thanks-message">
                  Your order total is ₹
                {' '}
                {this.props.amount}
              </p>
              <div className="OTP-remarks">
                <div className="OTP-add-button">
                  <Button
                    align="center"
                    color="Black"
                    text="Pay thru Cash"
                    type="simple"
                    onClickEvent={this.startPaymentByCash.bind(this)}
                    border
                  />
                </div>
              </div>
              <div className="OTP-add-button">
                <Button
                  align="center"
                  color="Blue"
                  text="Pay thru card"
                  type="simple"
                  onClickEvent={this.startPaymentByCredit.bind(this)}
                  border
                />
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
   store: state.user_session, menu: state.menu, cart: state.cart, order: state.order,
    customer: state.customer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    completeOrder
  }, dispatch); // createSession()(dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Pay);

