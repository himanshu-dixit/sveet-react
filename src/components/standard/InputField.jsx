/**
 * Standard Input Box
 * Props available - placeholder[string],type{simple,icon_input}
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './InputField.css';

class InputField extends Component {
  render() {
    const className = 'InputField';

    return (
      <input
        type={this.props.type}
        placeholder={this.props.placeholder}
        className={className}
        value={this.props.value || ''}
        onChange={this.props.onChangeValue}
      />
    );
  }
}

InputField.propTypes = {
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
};

export default InputField;
