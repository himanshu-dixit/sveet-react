/**
 * OTP verification Box
 * Props available - color
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './OTPBox.css';
import InputField from '../InputField';
import Button from '../Button';
import Error from '../Error/Error';
import { sendRequest } from '../../../services/APICall';

class OTPBox extends Component {
  constructor(props){
    super(props);
    this.state = {OTPSent: 0, timeOut: 30};
  }
  
  sendOTP(){
    let phone = Number(this.state.phone_no,10);
    let that = this;
    // Remove 0 if phone no. is present
    let phoneString = phone.toString().replace(/^0+/, '');
    if(phoneString.length !== 10 ){
      this.setState({error: true});
    }
    else{
        const requestJSON = {
          number: '+91'+that.state.phone_no,
        };
        sendRequest('sendOTP', requestJSON).then((data) => {
          if(data) {
            if (data.status === "success") {
              this.setState({OTPSent: 1});
              
              //Reset the timer
              that.setState({timeOut: 30})
              clearInterval(refreshIntervalId);
              // Start the counter
              let refreshIntervalId = setInterval(function () {
                that.setState({timeOut: that.state.timeOut-1});
                if(that.state.timeOut < 1){
                  clearInterval(refreshIntervalId);
                }
              }, 1000);
              
            }
          }
        });
      
    }
  }
  
  checkOTP(){
    let that = this;
    
    if(that.state.otp.toString().length === 4) {
      const requestJSON = {
        number: '+91' + that.state.phone_no,
        otp: Number(this.state.otp)
      };
      sendRequest('auth', requestJSON).then((data) => {
          if(data) {
            console.log(data);
            if (data.status === "success") {
              this.props.startUserSession(this.state.phone_no, data.id, data.token);
              this.setState({OTPSent: 1});
        
            }
            else{
              this.setState({OTPError: 1});
            }
          }
      }
      );
    }
    else{
      this.setState({OTPError: 1});
    }
    return true;
  }
  



  resendOTP(){
    this.setState({OTPSent: 0});
    this.setState({timeOut: 20});
  }
  
  addUsertoSession(){
    this.checkOTP()
  }
  
  phoneChange = e => {
    var text = e.target.value;
    this.setState({phone_no: text});
  }
  
  OTPChange = e => {
    var text = e.target.value;
    this.setState({otp: text});
    
  }
  
  render() {
    let that = this;
    return (
      <div className="Overlay">
        <div className="Overlay-box">
          { this.state.OTPSent === 0 ?
            <div>
          <div className="Overlay-heading">Enter Phone No  <div className="Close" onClick={()=>{this.props.OnDiscard()}}>✕</div> </div>
          <div className="OTP-container">
            <div className="OTP-remarks">
              <InputField
                type="text"
                placeholder="Enter your no"
                value={this.state.phone_no}
                onChangeValue={this.phoneChange}
              />
              { this.state.error ?
                <Error
                  text="Phone no is not valid"
                />
                : ''
              }
            </div>
            <div className="OTP-add-button">
              <Button
                align="center"
                color="Blue"
                text="Request OTP"
                type="simple"
                onClickEvent={that.sendOTP.bind(this)}
                border
              />
              
            </div>
          </div>
            </div> :
            <div>
              <div className="Overlay-heading">Enter OTP  <div className="Close" onClick={()=>{this.props.OnDiscard()}}>✕</div> </div>
              <div className="OTP-container">
                <div className="OTP-remarks">
                  <InputField
                    type="text"
                    placeholder="Enter your no"
                    value={this.state.otp}
                    onChangeValue={this.OTPChange}
                  />
                </div>
                {that.state.timeOut === 0 ?
                <div
                  className="OTP-Resend"
                  onClick={()=>{that.resendOTP()}}
                >Didn't receive OTP? Send Again</div>
                  :   <div
                    className="OTP-Retry-Invalid"
                  >Didn't reveive OTP? Send Again in {that.state.timeOut}</div>}
                <div className="OTP-add-button">
                  <Button
                    align="center"
                    color="Blue"
                    text="Verify OTP"
                    type="simple"
                    onClickEvent={that.addUsertoSession.bind(this)}
                    border
                  />
                </div>
                { this.state.OTPError ?
                  <Error
                    text="OTP is not valid"
                  />
                  : ''
                }

              </div>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default OTPBox;
