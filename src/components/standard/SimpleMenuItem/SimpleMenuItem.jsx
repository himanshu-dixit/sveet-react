/**
 * Simple Menu item component.
 * Props - name[String], description[String], price[Number]
 * @version 1.0.0
 * Date Created - 17/08/2018
 */

import React, { Component } from 'react';
import './SimpleMenuItem.css';
import PropTypes from 'prop-types';
import AddItemButton from '../AddItemButton/AddItemButton';

class SimpleMenuItem extends Component {
  render() {
    const description = this.props.description? this.props.description : '';
    return (
      <div className="Simple-item-menu-card">
        <div className="Simple-menu-details">

          <div className="Simple-item-meta">

            {
              this.props.isVeg === true ? (
                <img
                  src="https://i.imgur.com/kVTVaxd.png"
                  height="12px"
                  width="12px"
                  alt="veg"
                />
              ) : (
                <img
                  src="https://i.imgur.com/86tZP2e.png"
                  height="12px"
                  width="12px"
                  alt="veg"
                />
              )
            }

          </div>
          <div className="Simple-item-name">
            {this.props.name}
          </div>
          <div className="Simple-item-description">
            {description.length > 70 ? description.substr(0, 70)+'...' : description}
          </div>

          <div className="Simple-menu-bottom-bar">

            <div className="Simple-item-price">
              ₹
              {' '}
              {this.props.type === "review" ? this.props.item_count*this.props.price:this.props.price}
            </div>
            {!(this.props.show_button === 'no')
              ? (
                <div className="Simple-add-item-button">
                      <AddItemButton
                        item_id={this.props.item_id}
                        item_count={this.props.item_count}
                        OnClick={this.props.OnClick}
                        OnDelete={this.props.OnDelete}
                      />
                </div>
              )
              :         <div className="Simple-add-item-button">
                <AddItemButton
                  item_id={this.props.item_id}
                  item_count={this.props.item_count}
                  OnClick={this.props.OnClick}
                  OnDelete={this.props.OnDelete}
                />
              </div>
            }
          </div>

        </div>
      </div>
    );
  }
}

SimpleMenuItem.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
};

export default SimpleMenuItem;
