/**
 * Veg/Non-veg Filter for menu
 * @version 1.0.0
 * Date Created - 18/08/2018
 */

import React, { Component } from 'react';
import './VegFilter.css';


class VegFilter extends Component {
  constructor(props) {
    super(props);
    this.state = { veg: 0};

  }

  OnClick() {
    if (this.state.veg) {
      this.setState({ veg: 0 });
    }
    else {
      this.setState({ veg: 1 });
    }
    // Update status in user state
    this.props.OnClick();
  }

  render() {
    return (
      <span>
        {( (this.state.veg === 1 || this.props.currentState === true) && this.props.currentState !== false)
        ? (
          <img
            src={require('../../../../assets/images/Veg-filter.png')}
            className="Icon-button"
            onClick={this.OnClick.bind(this)}
            alt="Veg"
          />
        ) :           <img
            src={require('../../../../assets/images/Non-Veg-filter.png')}
            className="Icon-button"
            onClick={this.OnClick.bind(this)}
            alt="Non-Veg"
          />
        }

      </span>
    );
  }
}

VegFilter.propTypes = {};

export default VegFilter;
