// Import react components
import React, { Component } from 'react';

// Import Screens
import './App.css';
import Menu from './screens/Menu/Menu';
import TableCode from './screens/TableCode/TableCode';
import OrderInfo from './screens/OrderInfo/OrderInfo';
import Review from './screens/Review/Review';

import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import thunk from 'redux-thunk';

import reducer from './reducers/Index';

// Import locastorage method for persisting user state
import { loadState, saveState } from './services/LocalStorage';
import throttle from 'lodash/throttle';

const persistedState = loadState()

export const store = createStore(
  reducer,
  persistedState.data ,
  applyMiddleware(logger, thunk),
);

// Saving redux state to local storage
store.subscribe(throttle(() => {
  console.log('saveState')
  console.log(store.getState())
  saveState({
    data: store.getState()
  })
}, 1000));

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="App-Container">
          <Router>
            <Switch>
              <Route
                exact
                path="/"
                component={TableCode}
               />
              <Route exact path="/menu/:table_code" component={Menu} />
              <Route exact path="/order" component={OrderInfo} />
              <Route exact path="/review" component={Review} />
            </Switch>
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
