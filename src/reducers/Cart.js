/**
 * Reducer to manage cart and its items.
 * @version 1.0.0
 * Date Created - 22/08/2018
 */

import {
  ADD_TO_CART, REMOVE_FROM_CART, EMPTY_CART,
} from '../actions/Index';

function findItemIDBYCustomization(cart, itemId, customizations) {
  // Check in featured categories
  let itemCount = 0;
  let itemIndex = -1;
  let matchIndex = -1;
  cart.forEach((item) => {
    itemIndex++;
    if (item.item.item_id === itemId && (JSON.stringify(item.customizations) === JSON.stringify(customizations) || customizations === "")) {
      itemCount++;
      matchIndex = itemIndex;
    }
  });
  return matchIndex;
}


export default function createSession(state = [], action) {
  switch (action.type) {
    case ADD_TO_CART: {
      const cart = [...state];
      const index = findItemIDBYCustomization(cart, action.item.item_id, action.customizations);
      if (index === -1) {
        return [
          ...state,
          {
            item: action.item,
            customizations: action.customizations ? action.customizations : [],
            item_count: 1,
          },
        ];
      }

      const newState = [...state];
      newState[index].item_count = newState[index].item_count + 1;
      return newState;
    }
      break;
    case EMPTY_CART: {
      const newState = [];
      return newState;
    }
      break;
    case REMOVE_FROM_CART: {
      const newState = [...state];
      const itemCount = newState[action.index].item_count;
      if (newState[action.index].item_count === 1) {
        newState.splice(action.index, 1);
      }
      else {
        newState[action.index].item_count = itemCount - 1;
      }
      console.log(newState);
      return newState;
    }
    default:
      return state;
  }
}
