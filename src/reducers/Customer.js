/**
 * Reducer to manage customer
 * @version 1.0.0
 * Date Created - 23/08/2018
 */

import { ADD_USER_DATA } from '../actions/Index';

export default function createSession(state = [], action) {
  switch (action.type) {
    case ADD_USER_DATA:
      return {
        phone: action.phone,
        user_id: action.user_id,
        token: action.token,
      };
    default:
      return state;
  }
}
