/**
 * Reducer to manage order details
 * @version 1.0.0
 * Date Created - 22/08/2018
 */

import { ADD_ITEM_TO_ORDER, CREATE_ORDER, EMPTY_ORDER } from '../actions/Index';

export default function createSession(state = [], action) {
  switch (action.type) {
    case CREATE_ORDER:
      return action.data;
    case ADD_ITEM_TO_ORDER:
      return [...state, action.item_data];
    case EMPTY_ORDER: {
      const newState = [];
      return newState;
    }
    default:
      return state;
  }
}
