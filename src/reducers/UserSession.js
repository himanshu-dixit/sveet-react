/**
 * Reducer to deal with individual customer sessions
 * @version 1.0.0
 * Date Created - 22/08/2018
 */

import {
  CREATE_SESSION, UPDATE_CLIENT_STATUS, UPDATE_ORDER_ID, UPDATE_USER_LOGIN,
} from '../actions/Index';

export default function createSession(state = '', action) {
  switch (action.type) {
    case CREATE_SESSION: {
      return {
        store: action.data,
        user_logged_in: action.user_logged_in,
        table_code: action.table_code,
        order_id: '',
        clientstatus: 'true',
      };
    }
    case UPDATE_USER_LOGIN: {
      const prevState = { ...state };
      prevState.user_logged_in = action.user_logged_in;
      return prevState;
    }
    case UPDATE_CLIENT_STATUS: {
      const prevState = { ...state };
      prevState.clientstatus = action.data;
      return prevState;
    }
    case UPDATE_ORDER_ID: {
      const prevState = { ...state };
      prevState.order_id = action.order_id;
      return prevState;
    }
    default:
      return state;
  }
}
