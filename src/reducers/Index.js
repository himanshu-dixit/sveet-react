/**
 * Single point to combine all other reducers and manage single state tree.
 * @version 1.0.0
 * Date Created - 22/08/2018
 */

import { combineReducers } from 'redux';
import UserSession from './UserSession';
import Menu from './Menu';
import Offers from './Offers';
import Customer from './Customer';
import Cart from './Cart';
import Order from './Order';
import Diners from './Diners';
import Payments from './Payments';
import Feedback from './Feedback';

// Root Reducer
const rootReducer = combineReducers({
  user_session: UserSession,
  menu: Menu,
  offers: Offers,
  customer: Customer,
  cart: Cart,
  order: Order,
  diners: Diners,
  payments: Payments,
  feedback: Feedback,
});

export default rootReducer;
