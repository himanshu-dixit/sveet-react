/**
 * Reducer to manage menu
 * @version 1.0.0
 * Date Created - 22/08/2018
 */

import { CHANGE_VEG_FILTER, CREATE_MENU } from '../actions/Index';

export default function createSession(state = [], action) {
  switch (action.type) {
    case CREATE_MENU: {
      return action.data;
    }
    case CHANGE_VEG_FILTER: {
      const prevState = { ...state };
      prevState.is_veg_only = action.data;
      return prevState;
    }
    default:
      return state;
  }
}
