/**
 * Reducer to deal with Payment for order.
 * @version 1.0.0
 * Date Created - 22/08/2018
 */

import { CREATE_SESSION } from '../actions/Index';

export default function createSession(state = [], action) {
  switch (action.type) {
    case CREATE_SESSION:
      return {
      };
    default:
      return state;
  }
}
