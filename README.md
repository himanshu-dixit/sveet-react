# Coding Guidelines

The project was created using [Creat-React-Native-App](https://github.com/facebookincubator/create-react-app)

We're following AirBNB's coding guideline, it can be found out at https://github.com/airbnb/javascript/tree/master/react and ES6 to be followed.

**Tests:-**

Every code that we write must have unit tests accompanied by tests. 

Functional test using [Jest](https://jestjs.io/), to be written at the same time.

E2E using [Nightwatch.js](http://nightwatchjs.org/), to be written after completion of module (Using a story)

## Tips and recommendations
1.) Use yarn/npx over npm.

2.) The code is based on node 10

## Pushing the code
1.) Run ``npm run lint`` && ``npm run test``

2.) Use yarn/npx over npm.

## Libraries
1.) Axiom - To make HTTP requests [Converts data into  JSON].

2.) Redux - State management works well with react.S
